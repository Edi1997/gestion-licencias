<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('titulo');
            $table->String('equipoReportado');
            $table->bigInteger('idTipoReporte')->unsigned()->nullable();
            $table->foreign('idTipoReporte')->references('id')->on('tipoReporte')->onDelete('cascade');

            $table->bigInteger('idCategoriaReporte')->unsigned()->nullable();
            $table->foreign('idCategoriaReporte')->references('id')->on('categoriaReporte')->onDelete('cascade');

            $table->bigInteger('idNivelUrgencia')->unsigned()->nullable();
            $table->foreign('idNivelUrgencia')->references('id')->on('nivelUrgencia')->onDelete('cascade');

            $table->bigInteger('idEstatus')->unsigned()->nullable();
            $table->foreign('idEstatus')->references('id')->on('estatus')->onDelete('cascade');

            $table->bigInteger('idUsuarioTicket')->unsigned()->nullable();
            $table->foreign('idUsuarioTicket')->references('id')->on('usuarioRol')->onDelete('cascade');  
            
            $table->bigInteger('idUsuarioAsignado')->unsigned()->nullable();
            $table->foreign('idUsuarioAsignado')->references('id')->on('usuarioRol')->onDelete('cascade');

            $table->text('justificacions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
