<?php

namespace App\Http\Controllers;

use App\Licencia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth; 


class LicenciaController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $licencias=Licencia::paginate(10);
        return view('licencia.licencias',compact('licencias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $licencias=Licencia::create([
            'nombre_programa' => $request['nombre_programa'],
            'digitos' => $request['digitos'],
            'version' => $request['version'],
            'subversion' => $request['subversion'],
           
    ]);
   
    return redirect()->route('altalicencias.index')->with('Success','Elementos guardados'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function show(Licencia $licencia)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $licencias=Licencia::findOrFail($id);
        return view('licencia/edit',compact('licencias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $licencias=Licencia::findOrFail($id);
        $licencias->nombre_programa=$request->nombre_programa;
        $licencias->digitos=$request->digitos;
        $licencias->version=$request->version;
        $licencias->subversion=$request->subversion;
        $licencias->save();
        return redirect('altalicencias');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Licencia  $licencia
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Licencia::destroy($id);
        return redirect('altalicencias');
    }
}
