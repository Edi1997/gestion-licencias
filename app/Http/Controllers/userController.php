<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Auth; 
use App\User;
use App\usuarioRol;
use App\Rol;

class userController extends Controller
{
    public function getViewRole($vista){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    public function index()
    {
        $usuarios = DB::table('users')
                    ->select('users.*','usuariorol.idUsuario','usuariorol.idRol')
                    ->join('usuariorol','users.id','=','usuariorol.idUsuario')    
                    ->get();

        
        return view(self::getViewRole('usuarios'),compact('usuarios'));
    }
    public function edit($id)
    {
        $user= User::findOrFail($id);
        $usuarioRol=usuarioRol::where('idUsuario','=',$id)->get();

        $roles=Rol::all();
        
        
        return view('admin.edit',compact('user','usuarioRol','roles'));
    }
    public function update(Request $request,  $id)
    {
        $datosEmpleado=request()->except(['_token','_method','idRol']);
        $rol=request('idRol');
        // DB::table('usuariorol')
        //         ->join('users','usuariorol.idUsuario','=','users.id')
        //         ->update
        //         ->get();
        usuariorol::where('idUsuario','=',$id)->update(array('idRol' =>$rol));


        User::where('id','=',$id)->update($datosEmpleado);
        $empleado=  User::findOrFail($id);
        return redirect('usuarios');
    }
    public function destroy(Request $request,$id)
    {
      
      User::destroy($id);
        return redirect('usuarios');
    }
    public function store(Request $request)
    {
        $datosEmpleado=request()->except('_token');
     
        $user = new User();
        $user->name =  $datosEmpleado['name'];
        $user->email =  $datosEmpleado['email'];
        $user->password =  Hash::make( $datosEmpleado['password']);
        $user->apellidoP =  $datosEmpleado['apellidoP'];
        $user->apellidoM = $datosEmpleado['apellidoM'];
        $user->usuario =  $datosEmpleado['name'];
        $user->extension =  $datosEmpleado['extension'];
        $user->edificio =  $datosEmpleado['edificio'];
        $user->departamento =  $datosEmpleado['departamento'];
        $user->activo =  $datosEmpleado['activo'];
        $user->save();

        $emailUsuario=$datosEmpleado['email'];
        $idRol=$datosEmpleado['rol'];
        $usuario= DB::table('users')
                    ->select('id')
                    ->where('users.email', ($emailUsuario))
                    ->get();

        // $usuariorol=new usuarioRol();
        // $usuariorol->idUsuario=$usuario[0]->id;
        // $usuariorol->idRol=$idRol;
        // $usuariorol->save();
        DB::table('usuariorol')
        ->insert(['idUsuario' => $usuario[0]->id, 'idRol' => $idRol]
            );

      return  redirect('usuarios');
    }
    public function esperaAprob()
    {
        $user=Auth::user()->activo;
        if(Auth::user()->activo==0){
            return view('usuario.esperaAprob');    
        }else{
            return redirect('seguimiento');
        }
        
    }

    //agregadas para el acceso y actualizacion de usuarios inactivos
    public function solicitudesUsuarios(){
         $solicitudes = DB::table('users')
        ->select('users.id as id','name', 'apellidoP','apellidoM', 'email', 'created_at', 'departamento', 
        'edificio', 'extension','usuariorol.id as idUsuarioRol')
        ->join('usuariorol','users.id','=','usuariorol.idUsuario')    
        ->where('activo', 0)
        ->where('usuariorol.idRol','=',5)
        ->get();

        return view('admin/solicitud-usuario', compact('solicitudes'));
    }


    public function filtrarSolicitud(Request $request){
       if(!(request('filtro')=='filtro')) {
        $filtro=request('filtro');
        $solicitudes=DB::table('users')
        ->select('id','name', 'apellidoP','apellidoM', 'email', 'created_at', 'departamento', 
        'edificio', 'extension')
        ->where('activo', 0)
        ->orderBy($filtro, 'ASC')
        ->get();
       }

       return view('admin/solicitud-usuario', compact('solicitudes'));

   }

public function updateAcceso(Request $request){
    
      $id=request('id');
      $accion=request('action');
      if($accion=='Aceptar'){
       User::where('id','=', $id)->update(['activo'=> 1]);
       //Inserta en usuarioRol

      }else{
        User::where('id','=', $id)->update(['activo'=> 2]);
      }

      return redirect('solicitudes-usuarios');

    }



}
