<?php

namespace App\Http\Controllers;
use App\Exports\CsvExport;
use App\Imports\CsvImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\ticket;
use App\estatus;
use App\usuarioRol;
use PDF;



class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getViewRole($vista){
        $user=Auth::user()->id;
        
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
			   ->get();
	      switch ($rol[0]->idRol) {
            case 1:
            $return=("admin.".$vista."");
            return ($return);
                // Administrador
            break;
            case 2:
            $return=("helpdesk.".$vista."");
            return ($return);
                // HelpDesk
            break;
            case 3:
            
            $return=("agente.".$vista."");
            return ($return);
                // Agente 1er nivel
            break;
            case 4:
            $return=("agente.".$vista."");
            return ($return);
                // Agente 2do nivel
            break;
            case 5:
            $return=("usuario.".$vista."");
            return ($return);
            break;
            default:
            $return=("usuario.".$vista."");
            return ($return);
            break;
        }
        
    }
    public function index()
    {
        $tickets = DB::table('ticket')
        //->join('users', 'ticket.idUsuarioTicket','=','users.id') -> Relacion con la BD
        ->select("ticket.id as idTicket","ticket.idUsuarioTicket", "nivelurgencia.nivel", "estatus.estatus", "ticket.titulo", "categoriareporte.categoriaReporte", "ticket.created_at", "ticket.updated_at", "ticket.idUsuarioAsignado","nivelurgencia.id as idUrgencia","nivelurgencia.nivel as urgencia", "estatus.id as idEstatus","estatus.estatus as estatus","categoriareporte.id as idCategoriaReporte","categoriareporte.categoriaReporte as categoriaReporte","ticket.created_at as fechaRecepcion", "ticket.updated_at as fechaFinalizacion","ticket.justificacions as justificacion","ticket.subCategoria")
        ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
        ->join('estatus','ticket.idEstatus','=','estatus.id')    
        ->join('nivelurgencia', 'ticket.idNivelUrgencia', '=', 'nivelurgencia.id')
        ->where('ticket.idEstatus','!=','3')
        ->get();

		
        $estados=estatus::all();
        $categorias=DB::table('categoriareporte')->select('categoriareporte.*')->get();
        $parametros=[
            "departamento"=>"",
            "encargado"=>"",
            "fecha"=>"yyyy-mm-dd",
            "estatus"=>"",
            "categoria"=>"",
            "prioridad"=>""
        ];
        $parametros=json_encode($parametros);
        $users= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM","rol.rol","usuariorol.id as usuariorolId","users.edificio")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('rol.id','=','3')->orwhere('rol.id','=','4')->orwhere('rol.id','=','1')
        ->get();

        $solicitantes= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM","rol.rol","usuariorol.id as usuariorolId","users.departamento","users.edificio")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('rol.id','=','5')->orwhere('rol.id','=','1')
        ->get();


        $ticketsAgente = DB::table('ticket')
        ->select("ticket.id as idTicket","ticket.idUsuarioTicket", "nivelurgencia.nivel", "estatus.estatus", "ticket.titulo", "categoriareporte.categoriaReporte", "ticket.created_at", "ticket.updated_at", "ticket.idUsuarioAsignado","nivelurgencia.id as idUrgencia","nivelurgencia.nivel as urgencia", "estatus.id as idEstatus","estatus.estatus as estatus","categoriareporte.id as idCategoriaReporte","categoriareporte.categoriaReporte as categoriaReporte","ticket.created_at as fechaRecepcion", "ticket.updated_at as fechaFinalizacion","ticket.justificacions as justificacion","ticket.subCategoria")
        ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
        ->join('estatus','ticket.idEstatus','=','estatus.id')    
        ->join('nivelurgencia', 'ticket.idNivelUrgencia', '=', 'nivelurgencia.id')
        ->join('usuariorol', 'ticket.idUsuarioAsignado','=','usuariorol.id')
        ->where('usuariorol.idUsuario','=',Auth::user()->id)->get();

        return view(self::getViewRole('index'), compact("tickets", "estados","parametros", "categorias","users","solicitantes","ticketsAgente"));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response 
     */
    public function create()
    {
        // Lleva al formulario para crear ticket
        $categorias=DB::table('categoriareporte')->select('categoriareporte.*')->get();
        return view(self::getViewRole('ticket'),compact("categorias"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    

    public function store(Request $request)
    {
        
        $tick=ticket::all();
        $id=($tick->count())+1;
        $folio="CC-".$id;
        $user=Auth::user();         
        $this->validate($request,[ 'selectEquipo'=>'required',
            'selectTipoReporte'=>'required',
            'selectUrgencia'=>'required',
            'detalles'=> 'required',
        ]);
        if ($request->get('selectEquipo')==4){
            $equipo=$request->get('inputEquipo');
        }else{
            $equipo=$request->get('selectEquipo');
        }
        $rol=DB::table('usuariorol')
        ->select('usuariorol.id')
        ->where('usuariorol.idUsuario', ($user->id))
        ->get();
        ticket::create([
            'titulo'=> $request->detalles,
            'equipoReportado'=>$equipo,
            'idTipoReporte'=> $request->selectTipoReporte,
            'idCategoriaReporte'=>$request->selectCategoria,
            'idNivelurgencia'=>$request->selectUrgencia,
            'idEstatus'=>1,
            'idUsuarioTicket'=>$rol[0]->id,
            'justificacions'=>$request->textUrgencia,
            'subCategoria'=>$request->subCategoria,
            'folioTicket'=>$folio,
            'updated_at'=>NULL,
            'descServicio'=>"asd",
            'servicioRealizado'=>"-",
            'materialUtilizado'=>NULL,

        ]);   
        return redirect()->route('tickets.create'); 
    }

    /**
     * Display the specified resource. 
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function rejected($id)
    {
        
        $ticket=ticket::findOrFail($id);
        ticket::where('id','=',$id)->update(array('idEstatus' =>3));
        return redirect('tickets');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $affected = DB::update('update ticket set 
            idCategoriaReporte = "'.$request->categorias.'", 
            idNivelurgencia="'.$request->urgencia.'" ,
            idUsuarioAsignado="'.$request->encargado.'",
            idEstatus="'.$request->estado.'",
            subCategoria="'.$request->subcategoria.'"
            where id = ?', [$request->idTicket]);

        $id=$request->idTicket;
       $reporte=DB::table('ticket')
        ->select('id', 'titulo', 'servicioRealizado', 'materialUtilizado')
        ->where('id', ($id))
        ->get();

        if($request->estado==8){
            return view('admin.reporteFinal', compact('reporte'));
        }else{
        return redirect()->route('tickets.index'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function filtrar( Request $request)
    {
        $estatus= $request->estatus;
        
        $tickets = DB::table('ticket')
        //->join('users', 'ticket.idUsuarioTicket','=','users.id') -> Relacion con la BD
        ->select("ticket.id as idTicket","ticket.idUsuarioTicket", "nivelurgencia.nivel", "estatus.estatus", "ticket.titulo", "categoriareporte.categoriaReporte", "ticket.created_at", "ticket.updated_at", "ticket.idUsuarioAsignado","nivelurgencia.id as idUrgencia","nivelurgencia.nivel as urgencia", "estatus.id as idEstatus","estatus.estatus as estatus","categoriareporte.id as idCategoriaReporte","categoriareporte.categoriaReporte as categoriaReporte","ticket.created_at as fechaRecepcion", "ticket.updated_at as fechaFinalizacion","ticket.justificacions as justificacion","ticket.subCategoria")
        ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
        ->join('estatus','ticket.idEstatus','=','estatus.id')    
        ->join('nivelurgencia', 'ticket.idNivelUrgencia', '=', 'nivelurgencia.id')
        ->where('ticket.id', "!=", '');
        if ($request->categoria!=0) {
            $tickets->where('ticket.idCategoriaReporte', ($request->categoria));
        }
        
        if (isset($request->fecha)) {
            $tickets->where('ticket.created_at', 'like', ($request->fecha."%") );
        }
        if ($request->estatus!=0) {
            $tickets->where('ticket.idEstatus', ($request->estatus) );
        }
        if (isset($request->departamento)) {
            $tickets->where('ticket.idDepartamento', ($request->departamento) );
        }
        if (isset($request->encargado)) {
            $tickets->where('ticket.idUsuarioAsignado', ($request->encargado) );
        }
        if (isset($request->prioridad)) {
            $tickets->where('ticket.idNivelurgencia', ($request->prioridad) );
        }
        

        $estados=estatus::all();
        $categorias=DB::table('categoriareporte')->select('categoriareporte.*')->get();
        $tickets=$tickets->get();
        // dd($tickets);
        $parametros=[

            "departamento"=>$request->departamento,
            "encargado"=>$request->encargado,
            "fecha"=>$request->fecha,
            "estatus"=>$request->estatus,
            "categoria"=>$request->categoria,
            "prioridad"=>$request->prioridad
        ];
        $parametros=json_encode($parametros);
        $users= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM","rol.rol","usuariorol.id as usuariorolId","users.edificio")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('rol.id','=','3')->orwhere('rol.id','=','4')->orwhere('rol.id','=','1')
        ->get();
        $solicitantes= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM","rol.rol","usuariorol.id as usuariorolId","users.departamento","users.edificio")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('rol.id','=','5')->orwhere('rol.id','=','1')
        ->get();


        $ticketsAgente = DB::table('ticket')
        ->select("ticket.id as idTicket","ticket.idUsuarioTicket", "nivelurgencia.nivel", "estatus.estatus", "ticket.titulo", "categoriareporte.categoriaReporte", "ticket.created_at", "ticket.updated_at", "ticket.idUsuarioAsignado","nivelurgencia.id as idUrgencia","nivelurgencia.nivel as urgencia", "estatus.id as idEstatus","estatus.estatus as estatus","categoriareporte.id as idCategoriaReporte","categoriareporte.categoriaReporte as categoriaReporte","ticket.created_at as fechaRecepcion", "ticket.updated_at as fechaFinalizacion","ticket.justificacions as justificacion","ticket.subCategoria")
        ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
        ->join('estatus','ticket.idEstatus','=','estatus.id')    
        ->join('nivelurgencia', 'ticket.idNivelUrgencia', '=', 'nivelurgencia.id')
        ->join('usuariorol', 'ticket.idUsuarioAsignado','=','usuariorol.id')
        ->where('usuariorol.idUsuario','=',Auth::user()->id)->get();
        return view(self::getViewRole('index'), compact("tickets", "estados","parametros", "categorias","users","solicitantes","ticketsAgente"));
        
    }
    public function listarTicket(){
        {
            $estados=estatus::all();
            $categorias=DB::table('categoriareporte')->select('categoriareporte.*')->get();
            $parametros=[

                "departamento"=>"",
                "encargado"=>"",
                "fecha"=>"yyyy-mm-dd",
                "estatus"=>"",
                "categoria"=>"",
                "prioridad"=>""
            ];
            $parametros=json_encode($parametros);
            $tickets=ticket::all();
            return response()->view('admin.index', compact("tickets", "estados", "parametros", "categorias"));
            
            
        }

    }

    public function seguimientoTicket(){
        $idUsuario=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.id')
        ->where('usuariorol.idUsuario', ($idUsuario))
        ->get();


        $tickets = DB::table('ticket')
        //->join('users', 'ticket.idUsuarioTicket','=','users.id') -> Relacion con la BD
        ->select("ticket.idUsuarioTicket", "nivelurgencia.nivel",  "ticket.equipoReportado",
            "estatus.estatus", "ticket.titulo", "categoriareporte.categoriaReporte", 
            "ticket.created_at", "ticket.updated_at", "ticket.idUsuarioAsignado",
            "nivelurgencia.id as idUrgencia","nivelurgencia.nivel as urgencia", 
            "estatus.id as idEstatus","estatus.estatus as estatus",
            "categoriareporte.id as idCategoriaReporte","categoriareporte.categoriaReporte as categoriaReporte",
            "ticket.created_at as fechaRecepcion", "ticket.updated_at as fechaFinalizacion","ticket.id as idTicket")
        ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
        ->join('estatus','ticket.idEstatus','=','estatus.id')    
        ->join('nivelurgencia', 'ticket.idNivelUrgencia', '=', 'nivelurgencia.id')
        ->join('usuariorol', 'ticket.idUsuarioTicket',"=", "usuariorol.id")
        ->where('ticket.idUsuarioTicket',$rol[0]->id)
        ->get();

        // dd($tickets);

        return view(self::getViewRole('seguimiento'), compact("tickets"));
        
    }
    public function solicitudes()
    {
        $usuario=Auth::user()->id;
        $usuarioRol=DB::table('usuariorol')
        ->select('id','idUsuario','idRol')
        ->where('usuariorol.idUsuario',($usuario))->get();

        $tickets=DB::table('ticket')
        //->join('users', 'ticket.idUsuarioTicket','=','users.id') -> Relacion con la BD
        ->select("ticket.idUsuarioTicket", "nivelurgencia.nivel",  "ticket.equipoReportado",
            "estatus.estatus", "ticket.titulo", "categoriareporte.categoriaReporte", 
            "ticket.created_at", "ticket.updated_at", "ticket.idUsuarioAsignado",
            "nivelurgencia.id as idUrgencia","nivelurgencia.nivel as urgencia", 
            "estatus.id as idEstatus","estatus.estatus as estatus",
            "categoriareporte.id as idCategoriaReporte","categoriareporte.categoriaReporte as categoriaReporte","ticket.created_at as fechaRecepcion", "ticket.updated_at as fechaFinalizacion")
        ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
        ->join('estatus','ticket.idEstatus','=','estatus.id')    
        ->join('nivelurgencia', 'ticket.idNivelUrgencia', '=', 'nivelurgencia.id')
        ->join('usuariorol', 'ticket.idUsuarioTicket',"=", "usuariorol.id")
        ->where('ticket.idUsuarioAsignado',$usuarioRol[0]->id)
        ->get();
        
        return view(self::getViewRole('solicitud-ticket'),compact("tickets"));
    }

    //Genera reportes
    public function reportesTickets()
    {
        return view ('admin.reportes');
    }
    public function crearReporte(Request $request){
        // ->where('rol.id','=','5')->orwhere('rol.id','=','1')
        $fechaini = $request->fechaini;
        $fechafin = $request->fechafin;
        $tickets=DB::table('ticket')
        ->select("ticket.id as id_ticket","ticket.titulo","ticket.equipoReportado","users.name","users.apellidoP","users.apellidoM","users.edificio","users.departamento","categoriareporte.categoriaReporte","estatus.estatus","usuariorol.id")
        ->join('usuariorol','ticket.idUsuarioTicket','=','usuariorol.id')
        ->join('users','users.id','=','usuariorol.idUsuario')
        ->join('categoriareporte','categoriareporte.id','=','ticket.idCategoriaReporte')
        ->join('estatus','estatus.id','=','ticket.idEstatus')
        ->where('ticket.created_at','>=',$fechaini)
        ->where('ticket.created_at','<=',$fechafin)
        ->get();
        $pdf=PDF::loadView('admin/pdf',compact('tickets'));

        return $pdf->download('reporte_de_tickets_en_'.$fechaini.'_a_'.$fechafin.'.pdf');
    }
    public function crearReporteSolicitud($id)
    {
        $ticket=DB::table('ticket')
             ->select("ticket.id as idTicket","ticket.idUsuarioTicket as Solicitante","ticket.titulo", "categoriareporte.categoriaReporte","ticket.idUsuarioAsignado as Asignado","categoriareporte.id as idCategoriaReporte","ticket.created_at as fechaRecepcion","ticket.servicioRealizado","ticket.materialUtilizado","ticket.folioTicket")
            ->join('categoriareporte', 'ticket.idCategoriaReporte','=','categoriareporte.id')
            ->where('ticket.id',$id)
            ->get();
         $idUsuarioRolAsignado=$ticket[0]->Asignado;
         $user= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('usuariorol.id','=',$idUsuarioRolAsignado)
        ->get();
        
        
        $idUsuarioRolSolicitante=$ticket[0]->Solicitante;
        $solicitante= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM","rol.rol","usuariorol.id as usuariorolId","users.departamento","users.edificio")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('usuariorol.id','=',$idUsuarioRolSolicitante)
        ->get();    
        $pdf=PDF::loadView('admin/pdfSolicitud',compact('ticket','user','solicitante'));
        return $pdf->download('reporte_de_tickets.pdf');
    }


    // SOlicitudes tickets
    public function solicitudes2(){
        $ticket = DB::table('ticket')
        ->select('ticket.*', 'tipoReporte','categoriaReporte','nivel','idUsuario','name','ticket.idNivelUrgencia')
        ->join('tiporeporte', 'tiporeporte.id', '=', 'ticket.idTipoReporte')
        ->join('categoriareporte', 'categoriareporte.id', '=', 'ticket.idCategoriaReporte')
        ->join('nivelurgencia', 'nivelurgencia.id', '=', 'ticket.idNivelUrgencia')
        ->join('usuariorol','usuariorol.id' , '=', 'ticket.idUsuarioTicket')
        ->join('users','users.id' , '=', 'idUsuario')
        ->where('ticket.idEstatus', '=',1)  
        ->get();


        $rol=DB::table('usuariorol')
        ->select('usuariorol.id','usuariorol.idUsuario')
        ->where('usuariorol.idUsuario', (Auth::user()->id))
        ->get();
        
        $ticketAgente = DB::table('ticket')
        ->select('ticket.*', 'tipoReporte','categoriaReporte','nivel','idUsuario','name','ticket.idNivelUrgencia')
        ->join('tiporeporte', 'tiporeporte.id', '=', 'ticket.idTipoReporte')
        ->join('categoriareporte', 'categoriareporte.id', '=', 'ticket.idCategoriaReporte')
        ->join('nivelurgencia', 'nivelurgencia.id', '=', 'ticket.idNivelUrgencia')
        ->join('usuariorol','usuariorol.id' , '=', 'ticket.idUsuarioTicket')
        ->join('users','users.id' , '=', 'idUsuario')
        ->where('ticket.idUsuarioAsignado', '=',$rol[0]->id)  
        ->get();

        
        $users= DB::table('users')
        ->select("users.id as idAgente", "users.name", "users.apellidoP","users.apellidoM","rol.rol","usuariorol.id as usuariorolId","users.edificio")
        ->join('usuariorol','usuariorol.idUsuario','=','users.id')
        ->join('rol','rol.id','=','usuariorol.idRol')
        ->where('rol.id','=','3')->orwhere('rol.id','=','4')->orwhere('rol.id','=','1')
        ->get();
        return view (self::getViewRole('solicitud-ticket'),compact('ticket','users','ticketAgente'));
    }

    public function aceptarTicket(Request $request){
        $idTicket=$request->ticket;
        $prioridad=$request->prioridad;
        $encargado=$request->selectEncargado;

        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', (Auth::user()->id))
        ->get();

        if((($rol[0]->idRol)==3)||(($rol[0]->idRol)==4)){
            $affected = DB::table('ticket')
                ->where('id',$idTicket)
                ->update(['idEstatus' => 6 ]);
        }else{


            if ($encargado==0) {
                $affected = DB::table('ticket')
                ->where('id',$idTicket)
                ->update(['idEstatus' => 2 ]);
            }else{
                $affected = DB::table('ticket')
                ->where('id',$idTicket)
                ->update(['idEstatus' => 2 ]);
                $affected = DB::table('ticket')
                ->where('id',$idTicket)
                ->update(['idUsuarioAsignado' => $encargado ]);
            }
            $affected = DB::table('ticket')
            ->where('id',$idTicket)
            ->update(['idnivelurgencia' => $prioridad ]);
        }


        return redirect('/solicitudes-tickets');
    }
    public function reporteFinal(){
        return view('admin.reporteFinal');
    }

    public function reporteUpdate(Request $request){
        ticket::where('id', '=', $request->id)->update(array('titulo' => $request->titulo,
        'servicioRealizado' => $request->servicioRealizado,'materialUtilizado' => $request->materialUtilizado));
        $id=$request->id;

        return redirect('reporteSolicitud/'.$id);
    }


//Licencias de usuario
    public function licencia(){
        return view('licencia.licencias');
    }
}
