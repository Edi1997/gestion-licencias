<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use App\usuarioRol;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected function redirectTo(){
        $user=Auth::user()->id;
        $rol=DB::table('usuariorol')
        ->select('usuariorol.idRol')
        ->where('usuariorol.idUsuario', ($user))
        ->get();
        switch ($rol[0]->idRol) {
            case 1:
                return 'tickets';
                // Administrador
                break;
            case 2:
                return 'tickets';
                // HelpDesk
                break;
            case 3:
                return 'tickets';
                // Agente 1er nivel
                break;
            case 4:
                return 'tickets';
                // Agente 2do nivel
                break;
            case 5:
                return ('esperaAprob');
                break;
            default:
                return 'seguimiento';
                # code...
                break;
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
         User::create([
            'name' => $data['name'],
            'apellidoP' => $data['apellidoP'],
            'apellidoM' => $data['apellidoM'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'departamento' => $data['departamento'],
            'extension' => $data['extension'],
            'edificio' => $data['edificio'],
            'usuario' => $data['name']
        ]);
         $usuarioNuevo=DB::table('users')
                    ->select('users.id')
                    ->where('users.email', ($data['email']))
                    ->get();
            usuarioRol::create([
            'idUsuario'=>$usuarioNuevo[0]->id,
            'idRol'=> '5',
            ]);
        return User::where('email', $data['email'])->firstOrFail();
    }
}
