<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class usuarioRol extends Model
{
	public $timestamps = false;
    public $table="usuarioRol";
    protected $fillable = ['idUsuario', 'idRol'];
}
