<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    //    
    public $table="ticket";
    
    protected $fillable = ['titulo', 'equipoReportado','idTipoReporte','subCategoria',
    'idCategoriaReporte','idNivelurgencia','idEstatus','idUsuarioTicket','justificacions','created_at','updated_at','folioTicket'];
}
