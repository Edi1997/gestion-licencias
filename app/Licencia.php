<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Licencia extends Model
{
    
    protected $fillable = [
        'nombre_programa', 'digitos', 'version','subversion',
     ];
}
