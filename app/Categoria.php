<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    public $table="categoriaReporte";
    protected $fillable = [
        'categoriaReporte', 'descripcion'
    ];

    public $timestamps = false;

}
