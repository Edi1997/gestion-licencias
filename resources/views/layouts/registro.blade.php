@include('layouts.app')
@extends('registro')

@section('registro')
<div class="container" style="margin-top: 100px;">
  <div class="card card-register mx-auto mt-5">
    <div class="card-header"> SOLICITAR ACCESO </div>
    <div class="card-body">
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-4">
              <div class="form-label-group">
                <input type="text" id="firstName" class="form-control" placeholder=" Nombre(s)" required="required" autofocus="autofocus" name="name">
                <label for="firstName"> Nombre </label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-label-group"> 
                <input type="text" id="lastName" class="form-control" placeholder="Apellido Paterno" required="required" name="apellidoP">
                <label for="lastName"> Apellido Paterno </label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-label-group">
                <input type="text" id="lastName" class="form-control" placeholder="Apellido Materno" required="required" name="apellidoM">
                <label for="lastName"> Apellido Materno </label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required" name="email">
                <label for="inputEmail"> Email </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="required" name="password">
                <label for="inputPassword"> Password </label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="form-label-group">
            <input type="text" id="departamento" class="form-control" placeholder="Departamento" required="required" name="departamento">
            <label for="departamento"> Departamento </label>
          </div>
        </div>
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="inputExtension" class="form-control" placeholder="Extensión" required="required" name="extension">
                <label for="inputExtension"> Extensión </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="inputEdificio" class="form-control" placeholder=" Edificio" required="required" name="edificio">
                <label for="inputEdificio"> Edificio </label>
              </div>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">
          {{ __('Register') }}
        </button>
      </form>
      <div class="text-center">
        <a class="d-block small mt-3" href="{{url('/')}}"> Ya tienes cuenta?</a>
      </div>
    </div>
  </div>
</div>
@endsection
