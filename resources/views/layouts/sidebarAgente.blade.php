@include('layouts.app')
@section('sidebar')
    <ul class="sidebar navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/tickets') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span> Inicio </span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" href="{{ url('/solicitudes-tickets') }}">
                <i class="fas fa-fw fa-sticky-note"></i>
                <span> Solicitudes de Tickets </span>
            </a>
            {{-- <div class="dropdown-menu" aria-labelledby="pagesDropdown" style="background-color: #212529; border: none;">
                <h6 class="dropdown-header">Login:</h6>
                <a class="dropdown-item" href="login.html"> Solicitudes </a>
                <div class="dropdown-divider"></div>
                <h6 class="dropdown-header"> Otras: </h6>
                <a class="dropdown-item" href="blank.html">Blank Page</a>
            </div> --}}
        </li>
    </ul>
@endsection
