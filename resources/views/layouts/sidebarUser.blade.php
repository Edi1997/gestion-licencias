@include('layouts.app')
@section('sidebar')
    <ul class="sidebar navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('tickets.create') }}">
            <i class="fas fa-fw fa-list"></i>
            <span> Generar Ticket </span></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/seguimiento') }}">
            <i class="fas fa-fw fa-table"></i>
            <span> Seguimiento </span></a>
        </li>
    </ul>
@endsection
