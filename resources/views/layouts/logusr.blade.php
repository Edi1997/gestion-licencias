<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="{{asset('css/materialize.min.css')}}"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <meta charset="utf-8">
      <script src="https://kit.fontawesome.com/75924fa61f.js"></script>
      <script type="text/javascript" src="{{asset('/jQuery/jquery-3.4.1.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/javascript.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/materialize.min.js')}}"></script>
      <script type="text/javascript" src="{{asset('/js/materialize.js')}}"></script>
</head>
<body>
    <div id="app">
        <section>
    <ul id="dropdown1" class="dropdown-content">
      <li ><h6 class="center">Ordenar por:</h6></li>
      <li class="divider"></li>
  <li><a href="/Laravel/blogWeb/public/post/OrderbyCateg">Categorias</a></li>
  <li><a href="/Laravel/blogWeb/public/post/OrderbyRecen">Recientes</a></li>
  <li><a href="/Laravel/blogWeb/public/post/OrderbyViews">Mas vistos</a></li>
  <li class="divider"></li>
  <li><a href="/Laravel/blogWeb/public/post/showAll">Ver todos</a></li>
</ul>
  </section>
    <div class="navbar-fixed">
  <nav class="" style="background-color: #00293C">
    <div class="container">
    <div class="nav-wrapper ">
      <a href="/Laravel/blogWeb/public/home" class="brand-logo mobile">Blog</a>
        
        
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li class="hoverable"><a href="/Laravel/blogWeb/public/home">Home</a></li>
        <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Publicaciones<i class="material-icons right">arrow_drop_down</i></a></li>
        <li class="hoverable"><a href="/Laravel/blogWeb/public/users/showAll">Miembros</a></li>
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="/Laravel/blogWeb/public/user/profile/{{Auth::user()->id}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{ Auth::user()->name }} <span class="caret"></span>
            </a>
        
        <li class="hoverable"><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Log out</a></li>
                

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
                </form>
                
        
        <!--<li>
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                            -->
      </ul>
    </div>
  </div>
</nav>
</div>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<footer class="page-footer" style="background-color: #00293C;">
  <br><br><br><br>
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h2 class="white-text">Programando la vida.</h2>
                <p class="grey-text text-lighten-4">“No te preocupes si no funciona bien. Si todo lo que hizo lo hiciera, estarías sin trabajo.” </p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Siguenos en nuestras redes sociales</h5>
                <div class="col l4 offset-l2 s12 m6">
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!">Facebook</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Twitter</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Instagram</a></li>
                  <li><a class="grey-text text-lighten-3" href="#!">Linkedin</a></li>
                </ul>
              </div>
              <div class="col l4 offset-l2 s12 m6">
                <ul>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-facebook-square fa-2x"></i></a></li>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-twitter-square fa-2x"></i></a></li>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-instagram fa-2x"></i></a></li>
                  <li><a class="grey-text text-lighten-3" href="#!"><i class="fab fa-linkedin fa-2x"></i></a></li>
                </ul>
              </div>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright Text
            </div>
          </div>
</footer>
</html>
