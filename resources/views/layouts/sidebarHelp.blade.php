@include('layouts.app')
@section('sidebar')
    <ul class="sidebar navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/tickets') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span> Inicio </span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link" href="{{ url('/solicitudes-tickets') }}">
                <i class="fas fa-fw fa-sticky-note"></i>
                <span> Solicitudes de Tickets </span>
            </a>
            
        </li>

    </ul>
@endsection
