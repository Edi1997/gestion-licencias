@include('layouts.app')

@section('footer')
    <footer class="sticky-footer">
        <div class="container my-auto">
            <div class="copyright text-center my-auto">
            <span> Ticket Manager 2019 </span>
            </div>
        </div>
    </footer>
@endsection
