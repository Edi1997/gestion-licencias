@include('layouts.app')

@section('bread-ticket')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Generar Ticket </li>
    </ol>
@endsection

@section('bread-index')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Ver Tickets </li>
    </ol>
@endsection

@section('bread-seguimiento')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Mis Tickets </li>
    </ol>
@endsection

@section('bread-acceso')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Solicitudes de Acceso </li>
    </ol>
@endsection

@section('bread-sticket')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Solicitudes de Tickets </li>
    </ol>
@endsection

@section('bread-usuarios')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Todos los Usuarios </li>
    </ol>
@endsection

@section('bread-perfil')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active"> Editar Perfil </li>
    </ol>
@endsection
@section('bread-categorias')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li class="breadcrumb-item active">Categorías</li>
    </ol>

@endsection
@section('bread-licencias')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="#"> Inicio </a>
        </li>
        <li  class="breadcrumb-item active">  Alta Licencias </li>
    </ol>
@endsection
