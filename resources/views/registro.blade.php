@include('layouts.scripts')
@include('layouts.footer')

<!DOCTYPE html>
    <html lang="en">
        <head>
            @include('layouts.head')
        </head>
        <body class="bg-dark">
            @yield('registro')
        </body>
    </html>
    @yield('scripts')
