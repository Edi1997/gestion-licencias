@extends('licenciass')
 @include('layouts.navbar')
@include('layouts.sidebarAdmin') 
@section('solicitudes-tabla')

                    <h5 class="modal-title text-center" id="exampleModalLabel"> EDITAR INFORMACIÓN </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ url('/altalicencias/'.$licencias->id)}}" method="post" enctype="multipart/form-data" >
               {{csrf_field()}}
                {{method_field('PATCH')}}
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="nombre_programa"> Nombre del programa: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="nombre_programa" name="nombre_programa" value="{{$licencias->nombre_programa}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="digitos">Ultimos 5 digitos de la Licencia:</label>
                            </div>
                            <div class="col-9">
                                <input required required class="form-control" id="digitos" name="digitos" value="{{$licencias->digitos}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="version">Versión: </label>
                            </div>
                            <div class="col-9">
                                <input required class="form-control" id="version"  name="version" value="{{$licencias->version}}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-3">
                                <label for="subversion"> Subversión (Solo si aplica): </label>
                            </div>
                            <div class="col-9">
                                <input  class="form-control" id="subversion" name="subversion" value="{{$licencias->subversion}}">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

