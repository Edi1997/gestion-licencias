@extends('solicitud-usuario')
@include('layouts.navbar')
@include('layouts.sidebarHelp')



@section('filtro')
<script type="text/javascript">
        function submitForm(){
            var selectButton = document.getElementById( 'submitFiltrar' );
                selectButton.click();
            }
</script>


<form method="POST" action="filtrar-solicitud">
    @csrf
    <div class="form-group">
        
            <select class="form-control col-xl-2 col-sm-6 mb-2" id="filtro" name="filtro" onchange="submitForm()">
                <option selected value="filtro">Filtrar...</option>
                <option value="created_at">Fecha</option>
                <option value="name">Solicitante</option>
                <option value="departamento">Departamento</option>
                <option value="edificio">Edificio</option>
                
            </select>
    </div>
    <input type="submit" name="submit" id="submitFiltrar" hidden="">
</form>
@endsection

@section('solicitudes')
    <div class="row">
    <?php $noSolicitud=1; ?>
    @foreach($solicitudes as $solicitud)
        <div class="col-xl-4 col-sm-6 mb-4">
            <div class="card bg-light o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-user-circle"></i>
                    </div>
                    <div class="mr-5 text-center"> Solicitud # {{$noSolicitud++}}</div>
                    <div class="mr-5"> Solicitante: {{$solicitud->name}} {{$solicitud->apellidoP}} {{$solicitud->apellidoM}} </div>
                    <div class="mr-5"> Departamento: {{$solicitud->departamento}} </div>
                    <div class="mr-5"> Edificio: {{$solicitud->edificio}} </div>
                    <div class="mr-5"> Correo: {{$solicitud->email}} </div>
                    <div class="mr-5"> Fecha: {{$solicitud->created_at}} </div>
                </div>
                <a class="card-footer clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud" data-id="{{$solicitud->id}}" data-name="{{$solicitud->name}} {{$solicitud->apellidoP}} {{$solicitud->apellidoM}}"
                data-email="{{$solicitud->email}}" data-edificio="{{$solicitud->edificio}}" data-departamento="{{$solicitud->departamento}}"
                data-extension="{{$solicitud->extension}}" data-created_at="{{$solicitud->created_at}}" data-solicitud={{$noSolicitud}}>
                    <span class="float-left" data-toggle="modal" data-target="#modalsolicitud" > Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
        @endforeach
    
    </div>
@endsection





@section('modalsolicitud')
    <div class="modal fade" id="modalsolicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header text-center">
            <div class="row" style="position:relative; left:25%">
                    <div class="col-6">
                       <h4 class="modal-title text-center"> SOLICITUD #</h4>
                    </div>
                    <div class="col-6">
                    <input readonly class="form-control-plaintext h4" type="text"  name="solicitud" id="solicitud" value="solicitud" style="position: relative; right:25%; width:30%;">
                    </div>
                </div>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="user/update" method="post">
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-3">
                        <b> Solicitante: </b>
                    </div>
                    <div class="col-9">
                        <input readonly class="form-control-plaintext" type="text"  name="name" id="name" value="name" style="height:70%;">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Correo: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="email" id="email" value="email" style="height:70%;">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Edificio: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="edificio" id="edificio" value="edificio" style="height:70%;">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Departamento: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="departamento" id="departamento" value="departamento" style="height:65%;">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Extensión: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="extension" id="extension" value="extension" style="height:70%;">
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Fecha: </b>
                    </div>
                    <div class="col-9">
                    <input readonly class="form-control-plaintext" type="text"  name="created_at" id="created_at" value="created_at" style="height:70%;">
                    </div>
                </div><br>
            </div>
            <input type="text" id="id" name="id" hidden>
          
            <div class="modal-footer">
              <input type="submit" name="action"  value="Rechazar" class="btn btn-danger" > 
              <input type="submit" name="action" value="Aceptar" class="btn btn-primary">
            </div>
              </form>
          </div>
        </div>
    </div>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="../../../public/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript">

    var ATTRIBUTES = ['id','name', 'email','edificio', 'departamento', 'extension', 'created_at', 'solicitud'];
                        
                        $('[data-toggle="modal"]').on('click', function (e) {
                         // convert target (e.g. the button) to jquery object
                         var $target = $(e.target);
                         // modal targeted by the button
                         var modalSelector = $target.data('target');
                         
                         // iterate over each possible data-* attribute
                         var i=0;
                         ATTRIBUTES.forEach(function (attributeName) {
                           // retrieve the dom element corresponding to current attribute
                           var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
                           var dataValue = $target.data(attributeName);
                           // if the attribute value is empty, $target.data() will return undefined.
                           // In JS boolean expressions return operands and are not coerced into
                           // booleans. That way is dataValue is undefined, the left part of the following
                           // Boolean expression evaluate to false and the empty string will be returned
                           $modalAttribute.text(dataValue || '');
                           if (dataValue==null) {
                               dataValue="";
                           }
                               document.getElementById(ATTRIBUTES[i]).value =dataValue;
                               i++;
                       });
                       });
</script>
@endsection



