@extends('seguimiento')
@include('layouts.navbarUsuario')
@include('layouts.sidebarNoAprob')

@section('progreso')
    <div class="text-center">
        <h3> TU CUENTA NO HA SIDO APROBADA </h3><br>
    </div>
    
@endsection

@section('tickets')
    <div class="card mb-3">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Espera tu aprobacion
        </div>
        <div class="card-body">
            <div class="table-responsive">
                Estamos verificando tu informacion, lo cual puede tardar 3 dias; o acude al centro de computo ubicado en el edificio AG para que te aprueben.
            </div>
        </div>
        
    <div class="card-footer small text-muted">Actualizado ayer a las 11:59 PM</div>
    </div>
@endsection

@section('top')
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
@endsection
