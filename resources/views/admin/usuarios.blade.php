@extends('usuarios')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')

@section('solicitudes-tabla')

<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Usuarios
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>

                        <th> Nombre </th>
                        <th> Correo </th>
                        <th> Edificio </th>
                        <th> Departamento </th>
                        <th> Extensión </th>
                        <th> Tipo </th>
                        <th> Opciones </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> Nombre </th>
                        <th> Correo </th>
                        <th> Edificio </th>
                        <th> Departamento </th>
                        <th> Extensión </th>
                        <th> Tipo </th>
                        <th> Opciones </th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach($usuarios as $usuario)
                    <tr>
                        <td>{{$usuario->name}} </td>
                        <td>{{$usuario->email}} </td>
                        <td>{{$usuario->edificio}} </td>
                        <td>{{$usuario->departamento}} </td>
                        <td>{{$usuario->extension}} </td>
                        <?php 
                        switch ($usuario->idRol) {
                            case '1':
                            echo '<td> Administrador </td>';
                            break;
                            case '2':
                            echo '<td> HelpDesk </td>';
                            break;
                            case '3':
                            echo '<td> Agente 1er nivel </td>';
                            break;
                            case '4':
                            echo '<td> Agente 2do nivel </td>';
                            break;
                            case '5':
                            echo '<td> Usuario </td>';
                            break;
                            default:
                            
                            break;
                        }
                        ?>
                        <td>
                           <a class="btn btn-warning" href="{{url('/usuarios/'.$usuario->id.'/edit')}}" class="secondary-content"><i class="fa fa-edit"></i></a></a>
                           <form method="post" action="{{url('/usuarios/'.$usuario->id)}}">
                               {{csrf_field()}}
                               {{method_field('DELETE')}}

                               <button   class="btn btn-danger" type="submit" onclick="return confirm('¿Borrar?');"><i class="fa fa-trash"></i></button>                 
                           </form>


                       </td>

                   </tr>
                   @endforeach

               </tbody>

           </table>
       </div>
   </div>
   <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
@endsection

@section('modaleditar')
<div class="modal fade" id="modaleditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> EDITAR INFORMACIÓN </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form >
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="nombre"> Nombre: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="nombre" name="name">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="correo"> Correo: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="correo" name="correo">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="edificio"> Edificio: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="edificio" name="edificio">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="departamento"> Departamento: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="departamento" name="departamento">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="extensión"> Extensión: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" type="numeric" id="extensión" name="extension">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="pass"> Contraseña: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="pass" name="password" type="text">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="tipo"> Tipo: </label>
                        </div>
                        <div class="col-9">
                            <select class="form-control" id="tipo" name="rol">
                                <option value="5"> Usuario </option>
                                <option value="2"> HelpDesk </option>
                                <option value="3"> Agente 1° Nivel </option>
                                <option value="4"> Agente 2° Nivel </option>
                                <option value="1"> Administrador</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                    <button type="submit" class="btn btn-primary"> Aceptar </button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('modalregistro')
<div class="modal fade" id="modalregistro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> AGREGAR USUARIO </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>


            <form action="{{url('/usuarios')}}" method="post" >
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="name"> Nombre: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="name" name="name" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="apellidoP"> A Paterno: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="apellidoP" name="apellidoP" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="apellidoM"> A Materno: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="apellidoM" name="apellidoM" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="email"> Correo: </label>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" id="email" name="email" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="pass"> Contraseña: </label>
                        </div>
                        <div class="col-9">
                            <input class="form-control" id="password" type="password" name="password" required="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="edificio"> Edificio: </label>
                        </div>
                        <div class="col-9">
                            <select placeholder="Edificio" class="form-control"style="height: 50px;" required="" name="edificio" id="edificio">
                              <option value="A">A</option>
                              <option value="B">B</option>
                              <option value="C">C</option>
                              <option value="CH">CH</option>
                              <option value="D">D</option>
                              <option value="F">F</option>
                              <option value="G">G</option>
                              <option value="H">H</option>
                              <option value="I">I</option>
                              <option value="J">J</option>
                              <option value="K">K</option>
                              <option value="L">L</option>
                              <option value="LL">LL</option>
                              <option value="M">M</option>
                              <option value="N">N</option>
                              <option value="Ñ">Ñ</option>
                              <option value="O">O</option>
                              <option value="P">P</option>
                              <option value="Q">Q</option>
                              <option value="R">R</option>
                              <option value="S">S</option>
                              <option value="T">T</option>
                              <option value="U">U</option>
                              <option value="V">V</option>
                              <option value="W">W</option>
                              <option value="X">X</option>
                              <option value="Y">Y</option>
                              <option value="Z">Z</option>
                              <option value="AA">AA</option>
                              <option value="AB">AB</option>
                              <option value="AC">AC</option>
                              <option value="AG">AG</option>
                          </select>
                      </div>
                  </div>
                  <div class="row form-group">
                    <div class="col-3">
                        <label for="departamento"> Departamento: </label>
                    </div>
                    <div class="col-9">
                        <select placeholder="Departamento" class="form-control" style="height: 50px;" required="" name="departamento" id="departamento">
                  <option value="Ciencias Económico - Administrativo">Ciencias Económico - Administrativo</option>
                  <option value="DEPI">DEPI</option>
                  <option value="Eléctrica">Eléctrica</option>
                  <option value="Gestión Empresarial">Gestión Empresarial</option>
                  <option value="Ingeniería Bioquimica">Ingeniería Bioquimica</option>
                  <option value="Ingeniería Industrial">Ingeniería Industrial</option>
                  <option value="Ingeniería en Materiales">Ingeniería en Materiales</option>
                  <option value="Ingeniería Mécanica">Ingeniería Mécanica</option>
                  <option value="Ingeniería Mecatrónica">Ingeniería Mecatrónica</option>
                  <option value="Posgrado de Eléctrica">Posgrado de Eléctrica</option>
                  <option value="Sistemas y Computación">Sistemas y Computación</option>
                </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-3">
                        <label for="extension"> Extensión: </label>
                    </div>
                    <div class="col-9">
                        <input class="form-control" id="extension" type="numeric" name="extension" required="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-3">
                        <label for="activo"> Activo: </label>
                    </div>
                    <div class="col-9">
                        <select class="form-control" id="activo" name="activo" required="">
                            <option value="0"> Inactivo </option>
                            <option value="1"> Activo</option>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-3">
                        <label for="tipo"> Tipo: </label>
                    </div>
                    <div class="col-9">
                        <select class="form-control" id="tipo" name="rol" required="">
                            <option value="5"> Usuario </option>
                            <option value="1"> Administrador</option>
                            <option value="2"> HelpDesk </option>
                            <option value="3"> Agente 1° Nivel </option>
                            <option value="4"> Agente 2° Nivel </option>

                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Aceptar </button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection

@section('top')
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection
