@extends('reportes')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')

@section('form')
    <div class="text-center">
        <h1> NUEVO REPORTE </h1>
    </div>
    <form action="{{action('TicketController@crearReporte')}}" method="POST" target="_blank">
        {{csrf_field()}}      
        <!-- <div class="form-group">
          <label for="exampleFormControlSelect1"> Tipo de reporte: </label>
          <select class="form-control" id="selecTipo" name="selecTipo"  required >
            <option value='0'> General </option>
            <option value='1'> Por fecha </option>
          </select>
        </div>               -->
        <div class="form-group">
          <label for="exampleFormControlSelect1" name="labelini" id="labelini" > Fecha inicial: </label>
          <input  type="date" class="form-control" name="fechaini" id="fechaini" required >
        </div>      
        <div class="form-group">
          <label for="exampleFormControlSelect1"  name="labelfin" id="labelfin" > Fecha final: </label>
          <input  type="date" class="form-control" name="fechafin" id="fechafin" required >
        </div>
        <input type="text" name="x" id="x" hidden>        
        <!-- <input  type="date" class="form-control" name="fechaactual" id="fechaactual" hidden > -->
        <div class="form-group">
            <button type="submit" id="boton" name="boton" class="btn btn-primary mb-2" style="width: 100%;"> Generar </button>
        </div>

    </form>
@endsection