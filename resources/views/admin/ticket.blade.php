@extends('ticket')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')

@section('solicitud')
    <div class="text-center">
        <h1> NUEVO TICKET </h1>
    </div>
    <form method="POST" action="{{route('tickets.store')}}">
        {{csrf_field()}}
        <div class="form-group">
          <label for="exampleFormControlSelect1"> Equipo a Reportar: </label>
          <select class="form-control" id="selectEquipo" name="selectEquipo" onchange="" required >
            <option value="Teléfono"> Teléfono </option>
            <option value="Impresora"> Impresora </option>
            <option value="Laptop / PC"> Laptop / PC </option>
            <option value="Otro"> Otro </option>
          </select>
        </div>
        <div class="form-group">            
          <input type="text" placeholder="Otro" class="form-control" id="inputEquipo" name="inputEquipo" value=" " required hidden=""  />
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1"> Tipo de Reporte: </label>
            <select class="form-control" id="selectTipoReporte" name="selectTipoReporte" onchange="" required>
                <option value="1"> Falla </option>
                <option value="3"> Solicitud </option>
                <option value="2"> Actividad </option>
                <option value="4"> Otro </option>
            </select>
        </div>
        <div class="form-group">            
          <input type="text" placeholder="Otro" class="form-control" id="inputtipoReporte" name="inputtipoReporte" value=" " required hidden="" />
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1"> Categoria de reporte: </label>
            <select class="form-control" id="selectCategoria" name="selectCategoria"  onchange="" required>
            <?php
            foreach ($categorias as $categoria) {
                echo "<option value='".$categoria->id."'>".$categoria->categoriaReporte."</option>";
            }
            ?>
            </select>
        </div>
        
        
        <div class="form-group">
            <label for="exampleFormControlSelect1"> Nivel de Urgencia: </label>
            <select class="form-control" id="selectUrgencia" name="selectUrgencia"  onchange="otro()" required>
                <option value="4"> Bajo </option>
                <option value="3" selected > Medio </option>
                <option value="2"> Alto </option>
                <option value="1"> Urgente </option>
            </select>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1" id="labelUrgencia"> Explique la urgencia </label>
          <textarea class="form-control"  rows="3" name="textUrgencia" id="textUrgencia" required> </textarea>
        </div>
        <div class="form-group">
          <label for="exampleFormControlTextarea1"> Ingrese los detalles del reporte </label>
          <textarea class="form-control" id="exampleFormControlTextarea1" name="detalles" rows="3" required></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary mb-2" style="width: 100%;"> Enviar </button>
        </div>
    </form>
@endsection

@section('top')
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
@endsection
