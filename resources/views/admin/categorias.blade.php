@extends('categoria')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')
@section('categoria-tabla')
@include('layouts.app')

<div id="listar"></div>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Categorias
    </div>
    <div class="card-body">
        <div class="table-responsive">
           <div class="text-right"><form action="agregarCategoria"><button type="submit" class="btn btn-primary"> Nueva categoría </button></form> </div><br>
                <script type="text/javascript" src="../../../public/js/jquery-3.4.1.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

            

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th> Categoria </th>
                        <th> Descripcion </th>
                        <th> Opciones </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> Categoria </th>
                        <th> Descripcion </th>
                        <th> Opciones </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php

                    foreach ($categorias as $categoria) {
                        echo "<tr>";

                        
                        echo "<td>".$categoria->categoriaReporte."</td>";
                        echo "<td>".$categoria->descripcion."</td>";
                       
                        echo "<td>";                        
                        echo '<form action="categoria/findCategoria" method="post" style="display:inline-block;"> ';
                        ?> @csrf 
                        <input type="text" name="id" value="{{$categoria->id}}" hidden>
                        <?php
                        echo   '<button type="submit" class="btn btn-warning" style="position:relative; left:75%; width: 40px; ">
                                <i class="fa fa-edit"></i>
                              </button> </form>';
                        echo '<button data-toggle="modal" data-target="#modaleliminar" data-id="'.$categoria->id.'" class="btn btn-danger"  style=" position:relative; width: 40px; left:35%;">
                                <i class="fa fa-trash  data-toggle="modal" data-target="#modaleliminar" data-id="'.$categoria->id.'"  ></i>
                              </button>';
                        echo "</td>";
                        echo "</tr>";
                        
                    }
                    ?>
                    <script type="text/javascript">
                        var ATTRIBUTES = ['id'];
                        
                     $('[data-toggle="modal"]').on('click', function (e) {

                      // convert target (e.g. the button) to jquery object
                      var $target = $(e.target);
                      // modal targeted by the button
                      var modalSelector = $target.data('target');
                      
                      // iterate over each possible data-* attribute
                      var i=0;
                      ATTRIBUTES.forEach(function (attributeName) {
                        // retrieve the dom element corresponding to current attribute
                        var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
                        var dataValue = $target.data(attributeName);
                        // if the attribute value is empty, $target.data() will return undefined.
                        // In JS boolean expressions return operands and are not coerced into
                        // booleans. That way is dataValue is undefined, the left part of the following
                        // Boolean expression evaluate to false and the empty string will be returned
                        $modalAttribute.text(dataValue || '');
                        if (dataValue==null) {
                            dataValue="";
                        }
                            document.getElementById(ATTRIBUTES[i]).value =dataValue;
                            
                            i++;
                    });
                    });
                    </script>


</tbody>
</table>
</div>
</div>
<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
@endsection


@section('modaleliminar')
<div class="modal fade" id="modaleliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> ELIMINAR CATEGORIA </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="categoria/delete" method="POST">    
                @csrf
                <div class="modal-body">
                    <label>¿Desea eliminar la categoría?</label>
                </div>
                
                <input type="text"  name="id"  id="id" hidden>
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Eliminar </button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection

@section('top')
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection


