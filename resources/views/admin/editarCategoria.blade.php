@extends('categoria')
@include('layouts.navbar')
@include('layouts.sidebarAdmin')
@include('layouts.app')


@section('categoria-tabla')
<div id="listar"></div>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Agregar categoría
    </div>
    <div class="card-body">
        <div class="table-responsive">
                <script type="text/javascript" src="../../../public/js/jquery-3.4.1.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

                <?php echo '<form action="update" method="post">' ?>
                @csrf
                <div class="modal-body center">
                    <div class="row form-group">
                        <div class="col-12">
                            <input type="text" name="id"  id="id" value="{{$categoria[0]->id}}" hidden="">
                            <label for="solicitante">Categoria:</p>
                        </div>
                        <div class="col-12">
                            <input type="text" class="form-control" name="categoria" id="categoria" value="{{$categoria[0]->categoriaReporte}}" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12">
                            <label for="solicitante">Descripcion:</p>
                        </div>
                        <div class="col-12">
                            <textarea type="text" class="form-control" name="descripcion" id="descripcion" > {{$categoria[0]->descripcion}}</textarea>
                        </div>
                    </div>
                   
                </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Aceptar </button>
            </div>
        </form>
                  



</table>
</div>
</div>
<div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
@endsection



@section('top')
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection


