@extends('pdf')

@section('tabla')    
<title>Solicitud de mantenimiento</title>
<img src="{{asset('bg1.png')}}" width="127%;" style="position: absolute; left: -5rem; top: -5rem;">
<div style="position: absolute; top: 7.4rem; left: 8.5rem; z-index: 2">
	<p>{{$ticket[0]->fechaRecepcion}}</p>
</div>
<div style="position: absolute; top: 7.4rem; left: 34rem; z-index: 2">
	<p>{{$ticket[0]->folioTicket}}</p>
</div>
<div style="position: absolute; top: 9.4rem; left: 12.5rem; z-index: 2">
	<p>{{$solicitante[0]->departamento}}</p>
</div>
<?php
switch ($ticket[0]->idCategoriaReporte) {
	case '1':
		# Hardware...
		echo '<div style="position: absolute; top: 16.0rem; left: 39.5rem; z-index: 2">
				<p>x</p>
			</div>';		
		break;
	case '2':
		# Software...
		echo '<div style="position: absolute; top: 17.0rem; left: 39.5rem; z-index: 2">
				<p>x</p>
			</div>';		
		break;
	case '8':
		# Telefonia...
		echo '<div style="position: absolute; top: 18.0rem; left: 39.5rem; z-index: 2">
				<p>x</p>
			</div>';		
		break;
	case '9':
		# Redes...
		echo '<div style="position: absolute; top: 20.0rem; left: 39.5rem; z-index: 2">
				<p>x</p>
			</div>';		
		break;
	case '10':
		# Internet...
		echo '<div style="position: absolute; top: 19.0rem; left: 39.5rem; z-index: 2">
				<p>x</p>
			</div>';		
		break;
	default:
		# Otros
		echo '<div style="position: absolute; top: 21.1rem; left: 39.5rem; z-index: 2">
				<p>x</p>
			</div>';		
		break;
}
?>
<div style="position: absolute; top: 25.8rem; left: 3.0rem; z-index: 2">
	<p>{{$ticket[0]->titulo}}</p>
</div>

<div style="position: absolute; top: 34.0rem; left: 3.0rem; z-index: 2">
	<p>{{$ticket[0]->servicioRealizado}}</p>
</div>
<div style="position: absolute; top: 41.5rem; left: 3.0rem; z-index: 2">
	<p>{{$ticket[0]->materialUtilizado}}</p>
</div>
<div style="position: absolute; top: 51.0rem; left: 2.8rem; z-index: 2">
	<p style="font-size: smaller;">{{$solicitante[0]->name}} {{$solicitante[0]->apellidoP}} {{$solicitante[0]->apellidoM}}</p>
</div>
<div style="position: absolute; top: 51.0rem; left: 15.5rem; z-index: 2">
	<p style="font-size: smaller;">{{$user[0]->name}} {{$user[0]->apellidoP}} {{$user[0]->apellidoM}}</p>
</div>

<br><br><br><br><br><br><br><br><br><br>

@endsection
