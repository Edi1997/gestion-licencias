@extends('pdf')

@section('tabla')
    <div class="content">
        <h1 class="display-5 text-center" >REPORTE DE TICKETS</h1>
    </div>
    <div class="content">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Título</th>
                    <th scope="col">Solicitante</th>
                    <th scope="col">Edificio</th>
                    <th scope="col">Departamento</th>
                    <th scope="col">Categoria</th>
                    <th scope="col">Estado</th>
                </tr>
            </thead>
            <tbody>
            @foreach($tickets as $ticket)
                <tr>
                    <th scope="row">{{$ticket->titulo}}</td>
                    <td>{{$ticket->name}}</td>
                    <td>{{$ticket->edificio}}</td>
                    <td>{{$ticket->departamento}}</td>
                    <td>{{$ticket->categoriaReporte}}</td>
                    <td>{{$ticket->estatus}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
