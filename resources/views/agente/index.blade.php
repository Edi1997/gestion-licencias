
<?php $edit=false; ?>


@extends('index')
@include('layouts.navbar')
@include('layouts.sidebarAgente')

@section('solicitudes-cards')
<div class="row">
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white bg-secondary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-comments"></i>
                </div>
                <div class="mr-5"> # Solicitudes Nuevas </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="{{ url('/solicitudes-tickets') }}">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Urgentes </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#" onclick="setPrioridad(1)">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white o-hidden h-100" style="background-color: orange;">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                    
                </div>
                <div class="mr-5"> # Prioridad Alta</div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#" onclick="setPrioridad(2)">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white o-hidden h-100" style="background-color: rgb(233, 230, 60);">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Prioridad Media </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#" onclick="setPrioridad(3)">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> # Prioridad Baja </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#" onclick="setPrioridad(4)">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <!-- <div class="col-xl-3 col-sm-6 mb-3">
        <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fas fa-fw fa-list"></i>
                </div>
                <div class="mr-5"> Finalizados </div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="#" onclick="setPrioridad(5)">
                <span class="float-left"> Ver </span>
                <span class="float-right">
                    <i class="fas fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div> -->
</div>
@endsection

@section('solicitudes-tabla')
<?php
$parametros=json_decode($parametros);   
echo "$parametros->estatus";
?>
<div id="listar"></div>
<div class="card mb-3">
    <div class="card-header">
        <i class="fas fa-table"></i>
        Tickets
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <div>
                <form action="filtrar" method="post">
                    @csrf
                    <div class="row form-group">
                        <div class="col-2">
                            <label for="departamento"> Departamento: </label>
                        </div>
                        <div class="col-2">
                            <label for="encargado"> Agente encargado: </label>
                        </div>
                        <div class="col-2">
                            <label for="fecha"> Fecha: </label>
                        </div>
                        <div class="col-2">
                            <label for="estatus"> Estatus: </label>
                        </div>
                        <div class="col-2">
                            <label for="categoria" id="categLabel"> Categoria: </label>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-2">
                            <input class="form-control" id="departamento" name="departamento" disabled="" onchange="submitForm()" value='<?php echo $parametros->encargado  ?>'>
                        </div>
                        <div class="col-2">
                            <input class="form-control" id="usuarioEncargado" name="encargado" disabled="" onchange="submitForm()" value='<?php echo $parametros->encargado  ?>'>
                        </div>
                        <div class="col-2">
                            <input class="form-control" id="fecha" name="fecha" type="date" onchange="submitForm()" value='<?php echo $parametros->fecha  ?>' >
                        </div>

                        <div class="col-2">
                            <select class="form-control" id="estatus" name="estatus" onchange="submitForm()">
                                <option value="0">Todos</option>
                                <?php
                                foreach ($estados as $estado) {

                                    if (($parametros->estatus)==($estado->id)) {
                                        echo "<option selected value='".$estado->id."'>".$estado->estatus."</option>";    
                                    }else{
                                        echo "<option  value='".$estado->id."'>".$estado->estatus."</option>";
                                    }
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-2">
                            <select class="form-control" id="categoria" name="categoria" onchange="submitForm()">
                                <option value="0">Todos</option>
                                <?php
                                foreach ($categorias as $categoria) {
                                    if (($parametros->categoria)==($categoria->id)) {
                                        echo "<option selected value='".$categoria->id."'>".$categoria->categoriaReporte."</option>";
                                    }
                                    else{
                                        echo "<option value='".$categoria->id."'>".$categoria->categoriaReporte."</option>";   
                                    }
                                }
                                ?>
                            </select>


                            <input class="form-control" id="prioridad" name="prioridad" onchange="submitForm()" value='<?php echo $parametros->prioridad ?>' hidden="">
                        </div>
                    </div>
                    <input type="submit" name="submit" id="submitFiltrar" hidden="">
                </form>
                <script type="text/javascript" src="../../../public/js/jquery-3.4.1.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

                <script type="text/javascript">
                    function submitForm(){

                        var selectButton = document.getElementById( 'submitFiltrar' );
                        selectButton.click();
                    }
                    function setPrioridad(prioridad){
                        var inputPrioridad = document.getElementById('prioridad');
                        inputPrioridad.value=prioridad;
                        submitForm();
                        
                    }

                    
                </script>
            </div>

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th> Solicitante </th>
                        <th> Prioridad </th>
                        <th> Estado </th>
                        <th> Descripción </th>
                        <th> Tipo </th>
                        <th> Fecha de recepción </th>
                        <th> Fecha de finalización </th>
                        <th> Encargado </th>
                        <th> Opciones </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th> Solicitante </th>
                        <th> Prioridad </th>
                        <th> Estado </th>
                        <th> Descripción </th>
                        <th> Tipo </th>
                        <th> Fecha de recepción </th>
                        <th> Fecha de finalización </th>
                        <th> Encargado </th>
                        <th> Opciones </th>
                    </tr>
                </tfoot>
                <tbody>
                    <?php

                    foreach ($ticketsAgente as $ticket) {
                        echo "<tr>";

                        
                        $bandera=false;
                        foreach ($solicitantes as $us){
                            if ($ticket->idUsuarioTicket==$us->usuariorolId){
                                echo "<td>".$us->name." ".$us->apellidoP." ".$us->apellidoM." - ".$us->departamento." - ".$us->edificio."</td>";
                                $bandera=true;
                                $solicitante=$us->name." ".$us->apellidoP." ".$us->apellidoM." - ".$us->departamento." - ".$us->edificio;
                                break;
                            }                            
                        }
                        if ($bandera==false){
                            echo "<td>"."Error"."</td>";
                        }
                        switch ($ticket->nivel) {
                            case 'Urgente':
                            echo '<td class="bg-danger"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Alta':
                            echo '<td style="background-color: orange;"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Media':
                            echo '<td style="background-color: rgb(233, 230, 60);"> '.$ticket->nivel.' </td>';
                            break;
                            case 'Baja':
                            echo '<td class="bg-primary"> '.$ticket->nivel.' </td>';
                            break;
                            default:
                            echo '<td class="bg-primary"> '.$ticket->nivel.' </td>';
                            break;
                        }
                        echo "<td>".$ticket->estatus."</td>";
                        echo "<td>".$ticket->titulo."</td>";
                        echo "<td>".$ticket->categoriaReporte."</td>";
                        echo "<td>".$ticket->created_at."</td>";
                        echo "<td>".$ticket->updated_at."</td>";
                        $bandera=false;
                        foreach ($users as $us){
                            if ($ticket->idUsuarioAsignado==$us->usuariorolId){
                                echo "<td>".$us->name." ".$us->apellidoP." ".$us->apellidoM." - ".$us->rol."</td>";
                                $bandera=true;
                                break;
                            }                            
                        }
                        if ($bandera==false){
                            echo "<td>"."SIN ASIGNAR"."</td>";
                        }
                        echo "<td>";                        
                        echo '<button class="btn btn-warning" data-toggle="modal" data-target="#modaleditar" data-ticket="'.$ticket->idTicket.'" data-urgencia="'.$ticket->idUrgencia.'" data-estado="'.$ticket->idEstatus.'" data-categorias="'.$ticket->idCategoriaReporte.'" data-encargado="'.$us->usuariorolId.'" data-recepcion="'.$ticket->fechaRecepcion.'" data-finalizacion="'.$ticket->fechaFinalizacion.'" data-justificacion="'.$ticket->justificacion.'" data-solicitante="'.$solicitante.'" data-subcategoria="'.$ticket->subCategoria.'" style="width: 40px;">
                                <i class="fa fa-edit"  data-toggle="modal" data-target="#modaleditar" data-ticket="'.$ticket->idTicket.'" data-urgencia="'.$ticket->idUrgencia.'" data-estado="'.$ticket->idEstatus.'" data-categorias="'.$ticket->idCategoriaReporte.'" data-encargado="'.$us->usuariorolId.'" data-recepcion="'.$ticket->fechaRecepcion.'" data-finalizacion="'.$ticket->fechaFinalizacion.'" data-justificacion="'.$ticket->justificacion.'" data-solicitante="'.$solicitante.'" data-subcategoria="'.$ticket->subCategoria.'"></i>
                              </button>';
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                    <script type="text/javascript">
                        var ATTRIBUTES = ['ticket','urgencia','estado' ,'categorias','encargado','recepcion','finalizacion','justificacion','solicitante',"subcategoria"];
                        
                     $('[data-toggle="modal"]').on('click', function (e) {

                      // convert target (e.g. the button) to jquery object
                      var $target = $(e.target);
                      // modal targeted by the button
                      var modalSelector = $target.data('target');
                      
                      // iterate over each possible data-* attribute
                      var i=0;
                      ATTRIBUTES.forEach(function (attributeName) {
                        // retrieve the dom element corresponding to current attribute
                        var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);
                        var dataValue = $target.data(attributeName);
                        // if the attribute value is empty, $target.data() will return undefined.
                        // In JS boolean expressions return operands and are not coerced into
                        // booleans. That way is dataValue is undefined, the left part of the following
                        // Boolean expression evaluate to false and the empty string will be returned
                        $modalAttribute.text(dataValue || '');
                        if (dataValue==null) {
                            dataValue="";
                        }
                            document.getElementById(ATTRIBUTES[i]).value =dataValue;
                            
                            i++;
                    });
                    });
                    </script>


</tbody>
</table>
</div>
</div>

</div>
@endsection

@section('modaleditar')
<div class="modal fade" id="modaleditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> EDITAR TICKET </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo '<form action="tickets/update" method="post">' ?>
                @csrf
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-9">
                            <input type="text" name="idTicket"  id="ticket" hidden="">
                            <label for="solicitante">Solicitante:</p>
                        </div>
                        <div class="col-9">
                            <input type="text" class="form-control" name="solicitante" id="solicitante" disabled="">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="prioridad"> Prioridad: </label>
                        </div>
                        <div class="col-9">
                            
                            <select class="form-control" id="urgencia" name="urgencia">
                                <option value="4"> Baja </option>
                                <option value="3"> Media </option>
                                <option value="2"> Alta </option>
                                <option value="1"> Urgente </option>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="estado"> Estado: </label>
                        </div>
                        <div class="col-9">
                            <select class="form-control" id="estado" name="estado">
                                <?php
                                foreach ($estados as $estado) {
                                    echo "<option  value='".$estado->id."'>".$estado->estatus."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="categoria"> Categoria: </label>
                        </div>
                        <div class="col-9">
                            <select class="form-control" id="categorias" name="categorias">
                                <?php
                                foreach ($categorias as $categoria) {
                                    echo "<option value='".$categoria->id."'>".$categoria->categoriaReporte."</option>";   
                                    
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                            <label for="subcategoria"> Subcategoria: </label>
                        </div>
                        <div class="col-9">
                           <input type="text" class="form-control" name="subcategoria" id="subcategoria">
                        </div>
                    </div>
                    <div class="row form-group">
                        
                        <div class="col-9">
                            <select class="form-control"  name="" disabled="" hidden="">
                                <?php
                                    foreach ($users as $us){
                                            if($us->idAgente==Auth::user()->id){
                                            echo "<option selected value='".$us->usuariorolId."'>".$us->name." ".$us->apellidoP." ".$us->apellidoM."-".$us->rol."</option>";
                                            }else{
                                                echo "<option  value='".$us->usuariorolId."'>".$us->name." ".$us->apellidoP." ".$us->apellidoM."-".$us->rol."</option>";
                                            }
                                    }
                                ?>
                            </select>
                            <input type="text" name="encargado" id="encargado" hidden=""> 
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-3">
                         Fecha de Recepción:
                     </div>
                     <div class="col-9">
                        <input type="text" class="form-control" disabled="" id="recepcion">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-3">
                        <label for="fechafinal"> Fecha de Finalización: </label>
                    </div>
                    <div class="col-9">
                        <input class="form-control" type="text" disabled="" id="finalizacion" placeholder="">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-3">
                        <label for="fechafinal"> Justificacion: </label>
                    </div>
                    <div class="col-9">
                        <input class="form-control" type="text" disabled="" id="justificacion" disabled="">
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Aceptar </button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection

@section('modaleliminar')
<div class="modal fade" id="modaleliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h5 class="modal-title text-center" id="exampleModalLabel"> EDITAR TICKET </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            
                @csrf
                <div class="modal-body">
                    
                </div>
            
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                <button type="submit" class="btn btn-primary"> Aceptar </button>
            </div>
        </form>
    </div>
</div>
</div>
@endsection

@section('top')
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@endsection
