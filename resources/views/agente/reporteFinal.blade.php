@extends('reporteFinal')
@include('layouts.navbar')
@include('layouts.sidebarAgente')
@section('reporteFinal')

                    <h5 class="modal-title text-center" id="exampleModalLabel"> REPORTE FINAL</h5>
                    
                </div>
                </br>
                <form action="{{url('/reporte/update')}}" method="post" enctype="multipart/form-data" >
               {{csrf_field()}}
                    <input id="id" name="id" value="{{$reporte[0]->id}}" hidden>
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-6">
                                <label for="name">  Descripción del servicio que solicita o falla a reparar </label>
                            </div>
                            <div class="col-12">
                                <textarea class="form-control" id="titulo" name="titulo" required>{{$reporte[0]->titulo}}</textarea>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-6">
                                <label for="apellidoP"> Trabajo o servicio realizado</label>
                            </div>
                            <div class="col-12">
                                <textarea class="form-control" id="servicioRealizado" name="servicioRealizado" required>{{$reporte[0]->servicioRealizado}}</textarea>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-6">
                                <label for="apellidoP"> Material utilizado</label>
                            </div>
                            <div class="col-12">
                                <textarea class="form-control" id="materialUtilizado" name="materialUtilizado" >{{$reporte[0]->materialUtilizado}}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal"> Cancelar </button>
                        <button type="submit" class="btn btn-primary"> Aceptar </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endsection

