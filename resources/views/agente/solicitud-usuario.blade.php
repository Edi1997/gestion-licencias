@extends('solicitud-usuario')
@include('layouts.navbar')
@include('layouts.sidebarAgente')

@section('filtro')
    <div class="dropdown">
        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Ordenar por:
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#"> Fecha </a>
            <a class="dropdown-item" href="#"> Solicitante </a>
            <a class="dropdown-item" href="#"> Departamento </a>
            <a class="dropdown-item" href="#"> Edificio </a>
        </div>
    </div>
@endsection

@section('solicitudes')
    <div class="row">
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card bg-light o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-user-circle"></i>
                    </div>
                    <div class="mr-5 text-center"> # Solicitud </div>
                    <div class="mr-5"> Solicitante: - </div>
                    <div class="mr-5"> Departamento: - </div>
                    <div class="mr-5"> Correo: - </div>
                    <div class="mr-5"> Fecha: - </div>
                </div>
                <a class="card-footer clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud">
                    <span class="float-left" data-toggle="modal" data-target="#modalsolicitud"> Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card bg-light o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-user-circle"></i>
                    </div>
                    <div class="mr-5 text-center"> # Solicitud </div>
                    <div class="mr-5"> Solicitante: - </div>
                    <div class="mr-5"> Departamento: - </div>
                    <div class="mr-5"> Correo: - </div>
                    <div class="mr-5"> Fecha: - </div>
                </div>
                <a class="card-footer clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud">
                    <span class="float-left" data-toggle="modal" data-target="#modalsolicitud"> Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card bg-light o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-user-circle"></i>
                    </div>
                    <div class="mr-5 text-center"> # Solicitud </div>
                    <div class="mr-5"> Solicitante: - </div>
                    <div class="mr-5"> Departamento: - </div>
                    <div class="mr-5"> Correo: - </div>
                    <div class="mr-5"> Fecha: - </div>
                </div>
                <a class="card-footer clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud">
                    <span class="float-left" data-toggle="modal" data-target="#modalsolicitud"> Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card bg-light o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-user-circle"></i>
                    </div>
                    <div class="mr-5 text-center"> # Solicitud </div>
                    <div class="mr-5"> Solicitante: - </div>
                    <div class="mr-5"> Departamento: - </div>
                    <div class="mr-5"> Correo: - </div>
                    <div class="mr-5"> Fecha: - </div>
                </div>
                <a class="card-footer clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud">
                    <span class="float-left" data-toggle="modal" data-target="#modalsolicitud"> Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6 mb-3">
            <div class="card bg-light o-hidden h-100">
                <div class="card-body">
                    <div class="card-body-icon">
                        <i class="fas fa-fw fa-user-circle"></i>
                    </div>
                    <div class="mr-5 text-center"> # Solicitud </div>
                    <div class="mr-5"> Solicitante: - </div>
                    <div class="mr-5"> Departamento: - </div>
                    <div class="mr-5"> Correo: - </div>
                    <div class="mr-5"> Fecha: - </div>
                </div>
                <a class="card-footer clearfix small z-1" data-toggle="modal" data-target="#modalsolicitud">
                    <span class="float-left" data-toggle="modal" data-target="#modalsolicitud"> Ver </span>
                    <span class="float-right">
                        <i class="fas fa-angle-right"></i>
                    </span>
                </a>
            </div>
        </div>
    </div>
@endsection

@section('modalsolicitud')
    <div class="modal fade" id="modalsolicitud" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header text-center">
              <h5 class="modal-title text-center" id="exampleModalLabel"> SOLICITUD # </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-3">
                        <b> Solicitante: </b>
                    </div>
                    <div class="col-9">
                        Nombre Apellidos
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Correo: </b>
                    </div>
                    <div class="col-9">
                        xxx@xxxx.com
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Edificio: </b>
                    </div>
                    <div class="col-9">
                        A, AG, F, I, ...
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Departamento: </b>
                    </div>
                    <div class="col-9">
                        Sistemas, Centro de info.,  ...
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Extensión: </b>
                    </div>
                    <div class="col-9">
                        123, 567, 34, ...
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-3">
                        <b> Fecha: </b>
                    </div>
                    <div class="col-9">
                        xx / xx / xxxx
                    </div>
                </div><br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal"> Rechazar </button>
              <button type="button" class="btn btn-primary"> Aceptar </button>
            </div>
          </div>
        </div>
    </div>
@endsection
