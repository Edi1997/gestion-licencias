@include('layouts.scripts')
@include('layouts.footer')
@include('layouts.breadcrumb')

<!DOCTYPE html>
    <html lang="en">
        <head>
            @include('layouts.head')
        </head>
        <body id="page-top">
            {{-- NAVBAR --}}
            @yield('navbar')
            @yield('logout')
            {{-- CONTENT --}}
            <div id="wrapper">
                @yield('sidebar')
                <div id="content-wrapper">
                    <div class="container-fluid">
                        @yield('form')
                    </div>
                    @yield('footer')
                </div>
            </div>
            @yield('top')
            <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        </body>
        <script>
            $(document).ready(function(){           
                var d = new Date();                                                
                var strDate =d.getDate()  + "/" + (d.getMonth()+1) + "/" + d.getFullYear();   
                $('#fechaini').show(strDate);                   
                $('#boton').hide();
                $("#fechaini").change(function(){
                    var fechai=new Date($('#fechaini').val());
                    var fechaf=new Date($('#fechafin').val());
                    if (fechai.getTime()>fechaf.getTime()){
                        $('#boton').hide();                        
                    }else{ 
                        $('#boton').show();                        
                    }
                });
                $("#fechafin").change(function(){
                    var fechai=new Date($('#fechaini').val());
                    var fechaf=new Date($('#fechafin').val());
                    if (fechai.getTime()>fechaf.getTime()){
                        $('#boton').hide();                    
                    }else{
                        $('#boton').show();
                    }                    
                });
                
                    
            });
        </script>
    </html>
    @yield('scripts')
