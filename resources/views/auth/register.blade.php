@extends('layouts.app')

@section('content')
<div class="container" style="margin-top: 100px;">
  <div class="card card-register mx-auto mt-5">
    <div class="card-header"> SOLICITAR ACCESO </div>
    <div class="card-body">
      <form method="POST" action="{{ route('register') }}">
        @csrf
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-4">
              <div class="form-label-group">
                <input type="text" id="firstName" class="form-control" placeholder=" Nombre(s)" required="required" autofocus="autofocus" name="name">
                <label for="firstName"> Nombre </label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-label-group"> 
                <input type="text" id="lastName" class="form-control" placeholder="Apellido Paterno" required="required" name="apellidoP">
                <label for="lastName"> Apellido Paterno </label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-label-group">
                <input type="text" id="lastName" class="form-control" placeholder="Apellido Materno" required="required" name="apellidoM">
                <label for="lastName"> Apellido Materno </label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="required" name="email">
                <label for="inputEmail"> Email </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="required" name="password">
                <label for="inputPassword"> Password </label>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
            <label for="departamento"> Departamento </label>
            <select placeholder="Departamento" class="form-control" style="height: 50px;" required="" name="departamento" id="inputDepartamento">
                  <option value="Ciencias Económico - Administrativo">Ciencias Económico - Administrativo</option>
                  <option value="DEPI">DEPI</option>
                  <option value="Eléctrica">Eléctrica</option>
                  <option value="Gestión Empresarial">Gestión Empresarial</option>
                  <option value="Ingeniería Bioquimica">Ingeniería Bioquimica</option>
                  <option value="Ingeniería Industrial">Ingeniería Industrial</option>
                  <option value="Ingeniería en Materiales">Ingeniería en Materiales</option>
                  <option value="Ingeniería Mécanica">Ingeniería Mécanica</option>
                  <option value="Ingeniería Mecatrónica">Ingeniería Mecatrónica</option>
                  <option value="Posgrado de Eléctrica">Posgrado de Eléctrica</option>
                  <option value="Sistemas y Computación">Sistemas y Computación</option>
                </select>
            
          
        </div>
        <div class="form-group">
          <div class="form-row">
            <div class="col-md-6">
              <div class="form-label-group">
                <input type="text" id="inputExtension" class="form-control" placeholder="Extensión" required="required" name="extension">
                <label for="inputExtension"> Extensión </label>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-label-group">
                <select placeholder="Edificio" class="form-control"style="height: 50px;" required="" name="edificio" id="inputEdificio">
                  <option value="A">A</option>
                  <option value="B">B</option>
                  <option value="C">C</option>
                  <option value="CH">CH</option>
                  <option value="D">D</option>
                  <option value="F">F</option>
                  <option value="G">G</option>
                  <option value="H">H</option>
                  <option value="I">I</option>
                  <option value="J">J</option>
                  <option value="K">K</option>
                  <option value="L">L</option>
                  <option value="LL">LL</option>
                  <option value="M">M</option>
                  <option value="N">N</option>
                  <option value="Ñ">Ñ</option>
                  <option value="O">O</option>
                  <option value="P">P</option>
                  <option value="Q">Q</option>
                  <option value="R">R</option>
                  <option value="S">S</option>
                  <option value="T">T</option>
                  <option value="U">U</option>
                  <option value="V">V</option>
                  <option value="W">W</option>
                  <option value="X">X</option>
                  <option value="Y">Y</option>
                  <option value="Z">Z</option>
                  <option value="AA">AA</option>
                  <option value="AB">AB</option>
                  <option value="AC">AC</option>
                  <option value="AG">AG</option>
                </select>
                <!-- <label for="inputEdificio"> Edificio </label> -->
              </div>
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">
          {{ __('Registrar') }}
        </button>
      </form>
      <div class="text-center">
        <a class="d-block small mt-3" href="login"> ¿Ya tienes cuenta?</a>
      </div>
    </div>
  </div>
</div>

@endsection
