<?php

Route::get('/', function () {
    // return view('admin.index');
    return view ('layouts.login');
});
Route::post('/filtrar', 'TicketController@filtrar');
Route::get('/index', 'userController@index');
// Route::get('/index', function () {
//     return view('admin.index');
// });
Route::get('tickets/rejected/{id}', 'TicketController@rejected');

// Route::get('/usuarios', function () {
//     return view('admin.usuarios');
// });

Route::get('/nuevo-ticket', function () {
    return view('usuario.ticket');
});

//Sin autenticación
Route::get('seguimiento', 'TicketController@seguimientoTicket');
Route::get('esperaAprob', 'userController@esperaAprob');

Route::get('/perfil', function () {
    return view('layouts.perfil');
});

Route::get('/login', function () {
    return view('layouts.login');
});

Route::get('/registro', function () {
    return redirect('register');
});
//Agregadas para solicitud de acceso 3_12  
	Route::get('/solicitudes-usuarios', 'userController@solicitudesUsuarios');
	Route::post('/filtrar-solicitud', 'userController@filtrarSolicitud');
	Route::post('user/update', 'userController@updateAcceso');	
//Agregadas para el crud de categorias 4_12
    Route::get('/categorias', 'categoriaController@index');
    Route::post('categoria/delete', 'categoriaController@delete');
    Route::post('categoria/update', 'categoriaController@update');
    Route::post('categoria/store', 'categoriaController@store');
    Route::get('/agregarCategoria', function () {
        return view('admin.agregaCategoria');
    });
    Route::post('categoria/findCategoria', 'categoriaController@show');
//Agregadas para generar reportes
    Route::get('reportes', 'TicketController@reportesTickets');
    Route::post('reporteCreate', 'TicketController@crearReporte');
    Route::get('reporteSolicitud/{id}', 'TicketController@crearReporteSolicitud');
// Agregadas para la solicitud
    Route::get('/solicitudes-tickets', 'TicketController@solicitudes2');
    Route::post('/solicitudes-tickets/aceptarTicket', 'TicketController@aceptarTicket');

// rutas para el reporte de solicitud
    Route::get('/reporte', 'TicketController@reporteFinal');
    Route::post('/reporte/update', 'TicketController@reporteUpdate');

//rutas para las licencias
Route::resource('altalicencias','LicenciaController');


Route::post('tickets/update', 'TicketController@update');
Route::resource('tickets', 'TicketController');
Route::resource('usuarios', 'userController'); 
Route::get('/usuarios', 'userController@index');
Route::get('/home', function(){
	return redirect('tickets');
});
Auth::routes();

